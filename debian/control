Source: python-gabbi
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 9),
 dh-python,
 openstack-pkg-tools (>= 99~),
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-colorama,
 python3-coverage,
 python3-hacking,
 python3-httplib2,
 python3-jsonpath-rw,
 python3-mock,
 python3-pytest-cov,
 python3-six,
 python3-testtools,
 python3-wsgi-intercept,
 python3-yaml,
 subunit,
 testrepository,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/python/python-gabbi
Vcs-Git: https://salsa.debian.org/openstack-team/python/python-gabbi.git
Homepage: https://github.com/cdent/gabbi

Package: python-gabbi-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: declarative HTTP testing library - doc
 Gabbi is a tool for running HTTP tests where requests and responses are
 represented in a declarative form.
 .
 Gabbi works to bridge the gap between human readable YAML files that represent
 HTTP requests and expected responses and the obscured realm of Python-based,
 object-oriented unit tests in the style of the unittest module and its
 derivatives.
 .
 Each YAML file represents an ordered list of HTTP requests along with the
 expected responses. This allows a single file to represent a process in the
 API being tested. For example:
 .
  * Create a resource.
  * Retrieve a resource.
  * Delete a resource.
  * Retrieve a resource again to confirm it is gone.
 .
 At the same time it is still possible to ask gabbi to run just one request.
 If it is in a sequence of tests, those tests prior to it in the YAML file will
 be run (in order). In any single process any test will only be run once.
 Concurrency is handled such that one file runs in one process.
 .
 These features mean that it is possible to create tests that are useful for
 both humans (as tools for improving and developing APIs) and automated CI
 systems.
 .
 This package contains the documentation.

Package: python3-gabbi
Architecture: all
Depends:
 python3-colorama,
 python3-httplib2,
 python3-jsonpath-rw,
 python3-pbr,
 python3-six (>= 1.7.0),
 python3-testtools,
 python3-wsgi-intercept (>= 1.0.0),
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-gabbi-doc,
Description: declarative HTTP testing library - Python 3.x
 Gabbi is a tool for running HTTP tests where requests and responses are
 represented in a declarative form.
 .
 Gabbi works to bridge the gap between human readable YAML files that represent
 HTTP requests and expected responses and the obscured realm of Python-based,
 object-oriented unit tests in the style of the unittest module and its
 derivatives.
 .
 Each YAML file represents an ordered list of HTTP requests along with the
 expected responses. This allows a single file to represent a process in the
 API being tested. For example:
 .
  * Create a resource.
  * Retrieve a resource.
  * Delete a resource.
  * Retrieve a resource again to confirm it is gone.
 .
 At the same time it is still possible to ask gabbi to run just one request.
 If it is in a sequence of tests, those tests prior to it in the YAML file will
 be run (in order). In any single process any test will only be run once.
 Concurrency is handled such that one file runs in one process.
 .
 These features mean that it is possible to create tests that are useful for
 both humans (as tools for improving and developing APIs) and automated CI
 systems.
 .
 This package contains the Python 3.x module.
